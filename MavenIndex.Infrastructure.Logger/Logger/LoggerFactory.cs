namespace MavenIndex.Infrastructure.Logger
{
    public class LoggerFactory
    {
        public ILoggerConfig GetService(string key)
        {
            ILoggerConfig service = null;
            switch(key)
            {
                case "serilog":
                    service = new SerilogLoggerConfig();
                    break;                
                default:
                    break;
            }
            return service;
        }
    }
}