using MavenIndex.Infrastructure.Logger.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MavenIndex.Infrastructure.Logger
{
    public interface ILoggerConfig : IKey
    {
         public IApplicationBuilder ConfigureLogger(IApplicationBuilder app);
        public IHostBuilder ConfigureLogger(IHostBuilder builder);      

        void CloseLogger();
        public  IServiceCollection ConfigureLogger(IServiceCollection services, IConfiguration Configuration);
    }
}
