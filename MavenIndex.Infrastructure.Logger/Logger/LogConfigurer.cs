using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace MavenIndex.Infrastructure.Logger
{
 
    public static class LogConfigurer 
    {
       public static readonly LogCofigurationOptions Options = new LogCofigurationOptions();
       internal static readonly LoggerFactory LoggerConfigFactory = new LoggerFactory();
        
        public static IApplicationBuilder ConfigureLogger(this IApplicationBuilder app)
        {
            var iLogConfig =  LoggerConfigFactory.GetService(Options.Key);
            if (iLogConfig != null)
            {
                return iLogConfig.ConfigureLogger(app);
            }
            return app;


        }
        public static IHostBuilder ConfigureLogger(this IHostBuilder builder)
        {
            var iLogConfig =  LoggerConfigFactory.GetService(Options.Key);
            return iLogConfig.ConfigureLogger(builder);            
        }
        public static IServiceCollection ConfigureLogger(this IServiceCollection services, IConfiguration configuration)
        {
            var iLogConfig =  LoggerConfigFactory.GetService(Options.Key);
            if (iLogConfig != null)
            {
                return iLogConfig.ConfigureLogger(services, configuration);
            }
            return services;
        }
        
              
    }
}