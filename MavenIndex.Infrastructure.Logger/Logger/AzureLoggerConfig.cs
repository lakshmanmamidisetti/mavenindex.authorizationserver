using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging.AzureAppServices;
using MavenIndex.Infrastructure.Logger;

namespace API_Framework.Logger
{
    public class AzureLoggerConfig : ILoggerConfig
    {
        const string KEY = "azure";
        public string Key
        {
            get
            {
                return KEY;

            }
        }
        public IApplicationBuilder ConfigureLogger(IApplicationBuilder app) 
        {
                return app;
        }            
        public IHostBuilder ConfigureLogger(IHostBuilder builder)
        {
            return builder;
        }                 

        public void CloseLogger() 
        {

        }        
        public  IServiceCollection ConfigureLogger(IServiceCollection services, IConfiguration Configuration) =>            
         services.Configure<AzureFileLoggerOptions>(Configuration.GetSection("AzureFileLogging"));
    }
}