using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace MavenIndex.Infrastructure.Logger
{
    public class SerilogLoggerConfig : ILoggerConfig
    {

        const string KEY = "serilog";
        
        public IApplicationBuilder ConfigureLogger(IApplicationBuilder app) 
        {                      
            app.UseSerilogRequestLogging();
            return app;
        }

        public IHostBuilder ConfigureLogger(IHostBuilder builder)
        {
            builder.UseSerilog((hosTgContext, loggerConfiguration) => loggerConfiguration
            .ReadFrom.Configuration(hosTgContext.Configuration));             
            return builder;
        }
        
        public void CloseLogger() 
        {
             Log.CloseAndFlush();
        }
        
        public IServiceCollection ConfigureLogger(IServiceCollection services, IConfiguration Configuration) 
        {            
            return services;
        }  

        public string Key
        {
            get
            {
                return KEY;

            }
        }    
    }
}