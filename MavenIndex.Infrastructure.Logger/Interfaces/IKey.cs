﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenIndex.Infrastructure.Logger.Interfaces
{
    public interface IKey
    {
        string Key
        {
            get;
        }
    }
}
