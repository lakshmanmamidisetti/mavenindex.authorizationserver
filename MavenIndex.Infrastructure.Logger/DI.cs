﻿using Microsoft.Extensions.Configuration;
using System;

namespace MavenIndex.Infrastructure.Logger
{
    public static class DI
    {
        public static void AddLogger()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                        .AddJsonFile("appsettings.json", optional: false)
                        .AddJsonFile($"appsettings.{environmentName}.json", true, true)
                        .Build();
            LogConfigurer.Options.Key = config.GetSection("AppConfiguration").GetSection("LogKey").Value;      
            
        }

        public static void CloseLogger()
        {
            LogConfigurer.LoggerConfigFactory.GetService(LogConfigurer.Options.Key).CloseLogger();
        }
    }
}
