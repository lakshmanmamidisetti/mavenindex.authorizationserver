﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace MavenIndex.TestApp
{
    public static class TestHttpFunction
    {
        [FunctionName("TestHttpFunction")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            var userId = req.Headers["X-MS-CLIENT-PRINCIPAL-ID"].ToString();

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
           
            try
            {
                return new OkObjectResult(requestBody);
            }
            catch (Exception ex)
            {
                log.LogError($"{nameof(TestHttpFunction)}: {ex.Message}");
                return new BadRequestObjectResult($"{ex.Message}");
            }
        }
    }
}
