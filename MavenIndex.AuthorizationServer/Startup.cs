using AspNet.Security.OpenIdConnect.Primitives;
using MavenIndex.AuthorizationServer.Models;
using MavenIndex.Infrastructure;
using MavenIndex.Infrastructure.Logger;
using MavenIndex.Infrastructure.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
//using Microsoft.Azure.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using OpenIddict.Abstractions;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace MI.AuthorizationServer
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			_configuration = configuration;
			_serverConfiguration = new ServerConfiguration();
		}

		private readonly IConfiguration _configuration;
		private readonly ServerConfiguration _serverConfiguration;
		private const string CORSCONFIGURATION = "CORSCONFIGURATION";

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			AuthenticationConfiguration authenticationConfiguration = new AuthenticationConfiguration();
			_configuration.GetSection("AppConfiguration:AuthenticationConfiguration").Bind(authenticationConfiguration);

			_configuration.GetSection("AuthenticationServerConfiguration").Bind(_serverConfiguration);

			services.AddCors(options =>
			{
				options.AddPolicy(CORSCONFIGURATION, p =>
				{
				   p.WithOrigins(authenticationConfiguration.ClientUrls.Split(",")).AllowAnyHeader().AllowAnyMethod().AllowCredentials();
					
				});
			});

			services.AddControllers();

			services.AddDbContext<DbContext>(options =>
			{
				// Configure the context to use an in-memory store.
				options.UseInMemoryDatabase("AuthorizationServerDbContext");
				// Register the entity sets needed by OpenIddict.
				// Note: use the generic overload if you need
				// to replace the default OpenIddict entities.
				options.UseOpenIddict();
			});

			
			services.AddOpenIddict()
				// Register the OpenIddict core services.
				.AddCore(options =>
				{
					// Configure OpenIddict to use the EF Core stores/models.
					options.UseEntityFrameworkCore()
					   .UseDbContext<DbContext>();
				})
				// Register the OpenIddict server handler.
				.AddServer(options =>
				{
					// Register the ASP.NET Core MVC services used by OpenIddict.
					// Note: if you don't call this method, you won't be able to
					// bind OpenIdConnectRequest or OpenIdConnectResponse parameters.
					options.UseMvc();
					// Enable the token endpoint.
					// enable token and userinfo endpoints.                    
					options.EnableTokenEndpoint(_serverConfiguration.TokenApi);
					// Enable the password flow.
					options.AllowPasswordFlow();
					options.AllowRefreshTokenFlow();
					//options.AllowImplicitFlow();
					// Accept anonymous clients (i.e clients that don't send a client_id).
					options.AcceptAnonymousClients();

					options.SetAccessTokenLifetime(TimeSpan.FromHours(_serverConfiguration.ExpiryInHours));
					options.SetRefreshTokenLifetime(TimeSpan.FromHours(_serverConfiguration.RefreshTokenExpiryInHours));

					// Mark the "email", "profile" and "roles" scopes as supported scopes.
					options.RegisterScopes(
						OpenIddictConstants.Scopes.Roles,
						OpenIddictConstants.Scopes.OfflineAccess);
					options.EnableRequestCaching();

					// During development, you can disable the HTTPS requirement.
					if (true)
					{
						// During development, you can disable the HTTPS requirement.                        
						options.DisableHttpsRequirement();
						// Register an ephemeral signing key, used to protect the JWT tokens.
						// On production, you'd likely prefer using a signing certificate.
						// not for production, use x509 certificate and .AddSigningCertificate()                        
						options.AddEphemeralSigningKey(); // TODO: In production, use a X.509 certificate ?
					}

					// use jwt
					options.UseJsonWebTokens();
					// options.UseCustomTokenEndpoint();
				});


			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationConfiguration.Key));

			JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
			JwtSecurityTokenHandler.DefaultOutboundClaimTypeMap.Clear();
			services
				 .AddAuthentication(options =>
				 {
					 options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
				 })
				  .AddJwtBearer(options =>
				  {
					  options.Authority = authenticationConfiguration.Authority;
					  options.Audience = authenticationConfiguration.Audience;
					  options.RequireHttpsMetadata = false;
					  options.IncludeErrorDetails = true;
					  options.TokenValidationParameters = new TokenValidationParameters()
					  {
						  NameClaimType = OpenIdConnectConstants.Claims.Subject,
						  RoleClaimType = OpenIdConnectConstants.Claims.Role,
						  ValidateAudience = false,
						  ValidateIssuer = true,
						  ValidateLifetime = true,
						  IssuerSigningKeys = new List<SecurityKey>() { securityKey }
					  };
				  });

			// inject CV components
			services.ConfigureLogger(_configuration);
			//services.AddAuthorizationCommands();
			//services.AddAuthorizationInfrastructure();
			services.AddAppConfiguration(_configuration);
		    services.AddSwagger(_configuration);
			//services.AddAuhorizationDbProcedureInfrastructure();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, SwaggerConfiguration swaggerConfiguration)
		{
			app.UseAuthentication();

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			// add logger
			app.ConfigureLogger();

			app.UseRouting();

			app.UseCors(CORSCONFIGURATION);

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});

			//configure swagger
			app.ConfigureSwagger(swaggerConfiguration);
		}
	}
}
