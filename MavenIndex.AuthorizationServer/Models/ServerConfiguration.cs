﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MavenIndex.AuthorizationServer.Models
{
    public class ServerConfiguration
    {
        public int ExpiryInHours { get; set; }
        public int RefreshTokenExpiryInHours { get; set; }
        public string TokenApi { get; set; }
        public string RefreshTokenApi { get; set; }

    }
}
