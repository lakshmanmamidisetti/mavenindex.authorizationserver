﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MavenIndex.AuthorizationServer.Models
{
	public class GremlinClientConfiguration
	{
		public string Host { get; set; }
		public string Password { get; set; }
		public string Database { get; set; }
		public string Container { get; set; }
		public string EnableSSL { get; set; }
		public string Port { get; set; }

	}
}
