using FluentValidation.AspNetCore;
using MavenIndex.AuthorizationServer.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MavenIndex.AuthorizationServer.Extensions
{
    public static class FluetValidationExtension
    {
        public static IMvcBuilder AddValidator(IMvcBuilder builder)
        {
            builder.AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UserValidator>());        
            return builder;
        }
    }
}
