using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenIddict.Abstractions;
using OpenIddict.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Newtonsoft.Json;
using MavenIndex.AuthorizationServer.Helpers;
using MavenIndex.AuthorizationServer.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace MavenIndex.AuthorizationServer.Controllers
{
    [ApiController]
    [Route("connect")]
    public class AuthorizationController : ControllerBase
    {
        private readonly ILogger<AuthorizationController> _logger;
        private readonly IConfiguration _configuration;
        const string CLIENTID_CLAIM = "mi_clientid";
       
        public AuthorizationController(ILogger<AuthorizationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
     
        [HttpPost("token")]
        [Consumes("application/x-www-form-urlencoded")]
        public async Task<IActionResult> Connect([FromForm] Token data)
        {
            try
            {
                var scheme = OpenIddictServerDefaults.AuthenticationScheme;
                var request = HttpContext.GetOpenIdConnectRequest();
                GremlinClientConfiguration gremlinClientConfiguration = Utilities.GetGremlinClientConfiguration(_configuration);
                if (request.IsPasswordGrantType())
                {

                    // Validate the user credentials.
                    // Note: to mitigate brute force attacks, you SHOULD strongly consider
                    // applying a key derivation function like PBKDF2 to slow down
                    // the password validation process. You SHOULD also consider
                    // using a time-constant comparer to prevent timing attacks.
                   
                    UserVertex userVertex = await UserVertexData.GetMavenByEmail(request.Username, gremlinClientConfiguration);
                    if(userVertex != null && !string.IsNullOrEmpty(userVertex.Id))
                    {
                        return await GetToken(userVertex, scheme, request, null, true);
                    }
 
            
                }
                else if (request.IsRefreshTokenGrantType())
                {
                    // Retrieve the claims principal stored in the refresh token.
                    var info = await HttpContext.AuthenticateAsync(scheme);

                    UserVertex userVertex = await UserVertexData.GetMavenByMavenId(request.Username, gremlinClientConfiguration);
                    if (userVertex == null && string.IsNullOrEmpty(userVertex.Id))
                    {
                        var properties = new AuthenticationProperties(new Dictionary<string, string>
                        {
                            [OpenIdConnectConstants.Properties.Error] = OpenIdConnectConstants.Errors.InvalidGrant,
                            [OpenIdConnectConstants.Properties.ErrorDescription] = "The refresh token is no longer valid."
                        });

                        return Forbid(properties, scheme);
                    }
                   

                    return await GetToken(userVertex, scheme, request, (info.Ticket.Properties.ExpiresUtc.GetValueOrDefault() - DateTime.UtcNow), false);
                }

                //await Log(LogType.Info, $"unauthorized user access username {request.Username}!", 0);
                
            }
            catch(Exception ex)
            {
                _logger.LogCritical(ex,"Error Occured in Connect api");
            }
            return Unauthorized();
        }

       private async Task<IActionResult> GetToken(UserVertex user, string scheme, OpenIdConnectRequest request,TimeSpan? refreshTokenNewTimeSpan, bool setOfflineAccess)
        {
            var identity = new ClaimsIdentity(
                                scheme,
                                OpenIdConnectConstants.Claims.Name,
                                OpenIdConnectConstants.Claims.Role);
            // Add a "sub" claim containing the user identifier, and attach
            // the "access_token" desTation to allow OpenIddict to store it
            // in the access token, so it can be retrieved from your controllers.
            identity.AddClaim(OpenIdConnectConstants.Claims.Subject,
                user.Id.ToString(),
                OpenIdConnectConstants.Destinations.AccessToken);
            //identity.AddClaim(OpenIdConnectConstants.Claims.Name, user.LoginName,
            //    OpenIdConnectConstants.Destinations.AccessToken);
            identity.AddClaim(OpenIdConnectConstants.Claims.Role, "Maven",
                OpenIdConnectConstants.Destinations.AccessToken);
            identity.AddClaim(OpenIdConnectConstants.Claims.ClientId, "0",
               OpenIdConnectConstants.Destinations.AccessToken);
            identity.AddClaim(OpenIdConnectConstants.Claims.Scope, "Maven",
               OpenIdConnectConstants.Destinations.AccessToken);
            //identity.AddClaim(CLIENTID_CLAIM, user.ClientID.ToString(),
            //    OpenIdConnectConstants.Destinations.AccessToken);
            // ... add other claims, if necessary.
            var principal = new ClaimsPrincipal(identity);

            // Create a new authentication ticket holding the user identity.
            var ticket = new AuthenticationTicket(principal,
                new AuthenticationProperties(),
                scheme);

            // Set the list of scopes granted to the client application.
            // Note: the offline_access scope must be granted
            // to allow OpenIddict to return a refresh token.
            ticket.SetScopes(new[]
                {
                    OpenIdConnectConstants.Scopes.OpenId,                    
                    OpenIdConnectConstants.Scopes.OfflineAccess,
                    OpenIddictConstants.Scopes.Roles
                }.Intersect(request.GetScopes()));
            if (!setOfflineAccess)            
            {
                ticket.SetRefreshTokenLifetime(refreshTokenNewTimeSpan.GetValueOrDefault());
            }
            //ticket.SetResources("resource_server");

            foreach (var claim in ticket.Principal.Claims)
            {
                claim.SetDestinations(GetDesTations(claim, ticket));
            }
            //await Log(LogType.Info, "User Logged-in Successfully!", user.UserID);
            return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
        }

     
        private IEnumerable<string> GetDesTations(Claim claim, AuthenticationTicket ticket)
        {
            // Note: by default, claims are NOT automatically included in the access and identity tokens.
            // To allow OpenIddict to serialize them, you must attach them a desTation, that specifies
            // whether they should be included in access tokens, in identity tokens or in both.

            switch (claim.Type)
            {
                case OpenIdConnectConstants.Claims.Name:
                    yield return OpenIdConnectConstants.Destinations.AccessToken;

                    if (ticket.HasScope(OpenIdConnectConstants.Scopes.Profile))
                        yield return OpenIdConnectConstants.Destinations.IdentityToken;

                    yield break;

                case OpenIdConnectConstants.Claims.Email:
                    yield return OpenIdConnectConstants.Destinations.AccessToken;

                    if (ticket.HasScope(OpenIdConnectConstants.Scopes.Email))
                        yield return OpenIdConnectConstants.Destinations.IdentityToken;

                    yield break;

                case OpenIdConnectConstants.Claims.Role:
                    yield return OpenIdConnectConstants.Destinations.AccessToken;

                    if (ticket.HasScope(OpenIddictConstants.Scopes.Roles))
                        yield return OpenIdConnectConstants.Destinations.IdentityToken;

                    yield break;

                // Never include the security stamp in the access and identity tokens, as it's a secret value.
                case "AspNet.Identity.SecurityStamp": yield break;

                default:
                    yield return OpenIdConnectConstants.Destinations.AccessToken;
                    yield break;
            }
        }
       /* protected async Task Log(LogType logType, string message, int userId)
        {
            await _mediator.Send(new AddCommand<Log>
            {
                Object = new Log()
                {
                    CreateAtUtc = DateTime.UtcNow,
                    LogType = logType,
                    Message = message,
                    UserId = userId
                }
            });            
        }*/


    }

    public class Token
    {
        public string username { get; set; }
        public string password { get; set; }
        public string code { get; set; }
        public string grant_type { get; set; }

        public string scope { get; set; }
        public string refresh_token { get; set; }


    }

    
}
