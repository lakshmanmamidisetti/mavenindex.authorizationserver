using MavenIndex.Infrastructure.Logger;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;


namespace MI.AuthorizationServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
			DI.AddLogger();
			CreateHostBuilder(args).Build().Run();
			DI.CloseLogger();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
		 Host.CreateDefaultBuilder(args)

			 .ConfigureWebHostDefaults(webBuilder =>
			 {
				 webBuilder.UseStartup<Startup>();
			 })
			 .ConfigureLogger();

	}
}
