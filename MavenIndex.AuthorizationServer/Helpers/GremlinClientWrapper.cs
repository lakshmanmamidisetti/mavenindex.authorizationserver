﻿using Gremlin.Net.Driver;
using Gremlin.Net.Structure.IO.GraphSON;
using MavenIndex.AuthorizationServer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenIndex.AuthorizationServer.Helpers
{
	public class GremlinClientWrapper
	{
		private string Host;
		private string PrimaryKey;
		private string Database;
		private string Container;
		private int Port = 433;
		private bool EnableSSL = false;

		public GremlinClientWrapper(GremlinClientConfiguration gremlinClientConfiguration)
		{
			Host = gremlinClientConfiguration.Host;
			Port = Convert.ToInt32(gremlinClientConfiguration.Port);
			EnableSSL = Convert.ToBoolean(gremlinClientConfiguration.EnableSSL);
			Database = gremlinClientConfiguration.Database;
			Container = gremlinClientConfiguration.Container;
			PrimaryKey = gremlinClientConfiguration.Password;
		}



		public GremlinClient GetGremlinClient()
		{
			string containerLink = "/dbs/" + Database + "/colls/" + Container;
			var gremlinServer = new GremlinServer(
				Host,
				Port,
				enableSsl: EnableSSL,
				username: containerLink,
				password: PrimaryKey
			);
			return new GremlinClient(gremlinServer, new GraphSON2Reader(), new GraphSON2Writer(), GremlinClient.GraphSON2MimeType);
		}
	}
}
