﻿using MavenIndex.AuthorizationServer.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MavenIndex.AuthorizationServer.Helpers
{
    public class Utilities
    {
        public static GremlinClientConfiguration GetGremlinClientConfiguration(IConfiguration configuration)
        {
            GremlinClientConfiguration gremlinClientConfiguration = new GremlinClientConfiguration();
            configuration.GetSection("GremlinClientConfiguration").Bind(gremlinClientConfiguration);
            return gremlinClientConfiguration;
        }
    }
}
