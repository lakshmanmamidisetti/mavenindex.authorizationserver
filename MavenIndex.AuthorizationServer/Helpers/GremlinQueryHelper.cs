﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace MavenIndex.AuthorizationServer.Helpers
{
    public static class GremlinQueryHelper
    {
        public static string FindFocusGroupVertices(string userFocusGroupStatus)
        {
            return $".outE('{userFocusGroupStatus}')" +
                ".inV()" +
                ".hasLabel('focusgroup')";
        }

        public static string FindConnectionVertices()
        {
            return ".outE('connected.maven')" +
                ".inV()" +
                ".hasLabel('person')";
        }

        public static string GetUserVertex(this string userId)
        {
            return $"g.V('{userId}')"; 
        }

        public static string CreatePersonVertex(this string id)
        {
            return $"g.addV('person').property('id', '{id}')";
        }

        public static string GetVertexCount(this string label)
        {
            //check for firstName property to get better count
            return $"g.V().hasLabel('{label}').has('firstName').count()";
        }

      
    }
}
