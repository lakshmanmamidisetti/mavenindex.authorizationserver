﻿using MavenIndex.AuthorizationServer.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gremlin.Net.Driver;

namespace MavenIndex.AuthorizationServer.Helpers
{
    public class UserVertexData
    {
		public async static Task<UserVertex> GetMavenByEmail(string username, GremlinClientConfiguration gremlinClientConfiguration)
		{
			UserVertex userVertex = new UserVertex();
			try
			{
                
               GremlinClientWrapper gremlinClientWrapper = new GremlinClientWrapper(gremlinClientConfiguration);
                var gremlinClient = gremlinClientWrapper.GetGremlinClient();

                string filter = $@"g.V().has('person','email','{username}')";
                var result = await gremlinClient.SubmitAsync<dynamic>(filter);
               
                if (result != null && result.ToList().Count > 0)
                {
                    JObject jObject = JObject.Parse(JsonConvert.SerializeObject(result.FirstOrDefault()));
                    var id = (Dictionary<string, object>)result.FirstOrDefault();

                    //Mavens Data 
                    userVertex.Id = id["id"].ToString();
                    userVertex.FirstName = jObject["properties"] != null ? jObject["properties"]["firstName"] != null ? jObject["properties"]["firstName"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.LastName = jObject["properties"] != null ? jObject["properties"]["lastName"] != null ? jObject["properties"]["lastName"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.Email = jObject["properties"] != null ? jObject["properties"]["email"] != null ? jObject["properties"]["email"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.Phone = jObject["properties"] != null ? jObject["properties"]["phone"] != null ? jObject["properties"]["phone"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.State = jObject["properties"] != null ? jObject["properties"]["state"] != null ? jObject["properties"]["state"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.City = jObject["properties"] != null ? jObject["properties"]["city"] != null ? jObject["properties"]["city"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.ZipCode = jObject["properties"] != null ? jObject["properties"]["zipCode"] != null ? jObject["properties"]["zipCode"][0]["value"].ToString() : string.Empty : string.Empty;
                   
                }

            }
			catch (Exception ex)
			{

				throw ex;
			}

			return userVertex;
		}


        public async static Task<UserVertex> GetMavenByMavenId(string mavenId, GremlinClientConfiguration gremlinClientConfiguration)
        {
            UserVertex userVertex = new UserVertex();
            try
            {

                GremlinClientWrapper gremlinClientWrapper = new GremlinClientWrapper(gremlinClientConfiguration);
                var gremlinClient = gremlinClientWrapper.GetGremlinClient();

                string filter = $@"g.V('{mavenId}')";
                var result = await gremlinClient.SubmitAsync<dynamic>(filter);

                if (result != null && result.ToList().Count > 0)
                {
                    JObject jObject = JObject.Parse(JsonConvert.SerializeObject(result.FirstOrDefault()));
                    var id = (Dictionary<string, object>)result.FirstOrDefault();

                    //Mavens Data 
                    userVertex.Id = id["id"].ToString();
                    userVertex.FirstName = jObject["properties"] != null ? jObject["properties"]["firstName"] != null ? jObject["properties"]["firstName"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.LastName = jObject["properties"] != null ? jObject["properties"]["lastName"] != null ? jObject["properties"]["lastName"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.Email = jObject["properties"] != null ? jObject["properties"]["email"] != null ? jObject["properties"]["email"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.Phone = jObject["properties"] != null ? jObject["properties"]["phone"] != null ? jObject["properties"]["phone"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.State = jObject["properties"] != null ? jObject["properties"]["state"] != null ? jObject["properties"]["state"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.City = jObject["properties"] != null ? jObject["properties"]["city"] != null ? jObject["properties"]["city"][0]["value"].ToString() : string.Empty : string.Empty;
                    userVertex.ZipCode = jObject["properties"] != null ? jObject["properties"]["zipCode"] != null ? jObject["properties"]["zipCode"][0]["value"].ToString() : string.Empty : string.Empty;

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return userVertex;
        }

    }
}
