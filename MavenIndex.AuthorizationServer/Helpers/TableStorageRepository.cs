﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MavenIndex.AuthorizationServer.Helpers
{
    public class TableStorageRepository<T> : ITableStorageRepository<T> where T : TableEntity, new()
    {
        private readonly CloudTableClient _client;
        private string _tableName;

        public TableStorageRepository(string storageConnectionString, string overrideTableName = null) 
        {
            _tableName = overrideTableName ?? typeof(T).Name;
            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            _client = storageAccount.CreateCloudTableClient(new TableClientConfiguration());
        }

        public async Task<IEnumerable<T>> GetMany(string filter, int? take = 100)
        {
            var table = await GetTable();
            var query = new TableQuery<T>().Where(filter).Take(take);
            return table.ExecuteQuery(query);
        }

        public async Task<IEnumerable<T>> GetMany(string filter, EntityResolver<T> resolver, int? take = 100)
        {
            var table = await GetTable();
            var query = new TableQuery<T>().Where(filter).Take(take);
            return table.ExecuteQuery(query, resolver);
        }

        public async Task<T> GetSingle(string partitionKey, string rowKey)
        {
            var table = await GetTable();
            var query = BuildPointQuery(partitionKey, rowKey);
            return table.ExecuteQuery(query)?.SingleOrDefault();
        }

        public async Task<T> GetSingle(string partitionKey, string rowKey, EntityResolver<T> resolver)
        {
            var table = await GetTable();
            var query = BuildPointQuery(partitionKey, rowKey);
            return table.ExecuteQuery(query, resolver)?.SingleOrDefault();
        }

        public async Task<bool> UpsertRecord(T record)
        {
            var table = await GetTable();
            return (await table.ExecuteAsync(TableOperation.InsertOrReplace(record)))?.HttpStatusCode == (int)HttpStatusCode.NoContent;
        }

        public async Task<bool> DeleteSingle(string partitionKey, string rowKey)
        {
            var table = await GetTable();
            var record = await GetSingle(partitionKey, rowKey);

            if (record == null) return false;

            return (await table.ExecuteAsync(TableOperation.Delete(record)))?.HttpStatusCode == (int)HttpStatusCode.NoContent;
        }

        private async Task<CloudTable> GetTable()
        {
            var table = _client.GetTableReference(_tableName);
            if (!await table.ExistsAsync() && !table.CreateIfNotExists())
            {
                throw new Exception("Unable to create table.");
            }
            return table;
        }

        private TableQuery<T> BuildPointQuery(string partitionKey, string rowKey)
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition(nameof(TableEntity.PartitionKey), QueryComparisons.Equal, partitionKey),
                TableOperators.And,
                TableQuery.GenerateFilterCondition(nameof(TableEntity.RowKey), QueryComparisons.Equal, rowKey));
            return new TableQuery<T>().Where(filter);
        }
    }
}
