﻿using Gremlin.Net.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MavenIndex.AuthorizationServer.Helpers
{
    public static class GremlinQueryStore
    {

        public static async Task<int> GetVertexCount(GremlinClient gremlinClient,
            string label)
        {
            var results = await gremlinClient.SubmitAsync<Object>(
                GremlinQueryHelper.GetVertexCount(label)
                );
            int count = (int)(long)results.First<Object>();
            return count;
        }
    }
}
