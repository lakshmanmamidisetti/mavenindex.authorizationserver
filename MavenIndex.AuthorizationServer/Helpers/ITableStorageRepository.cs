﻿using Microsoft.Azure.Cosmos.Table;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MavenIndex.AuthorizationServer.Helpers
{
    public interface ITableStorageRepository<T> where T : ITableEntity, new()
    {
        public Task<IEnumerable<T>> GetMany(string filter, int? take = 100);
        public Task<IEnumerable<T>> GetMany(string filter, EntityResolver<T> resolver, int? take = 100);

        public Task<T> GetSingle(string partitionKey, string rowKey);
        public Task<T> GetSingle(string partitionKey, string rowKey, EntityResolver<T> resolver);

        public Task<bool> UpsertRecord(T record);

        public Task<bool> DeleteSingle(string partitionKey, string rowKey);
    }
}
