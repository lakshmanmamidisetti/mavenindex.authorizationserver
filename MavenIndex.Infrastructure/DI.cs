using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
//using Microsoft.Azure.Cosmos.Table;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using MavenIndex.Infrastructure.Models;

namespace MavenIndex.Infrastructure
{
    public static class DI
    {
       
        public static IServiceCollection AddAppConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            AppConfiguration appConfiguration = new AppConfiguration();
            configuration.GetSection("AppConfiguration").Bind(appConfiguration);
            services.AddSingleton<AppConfiguration>(appConfiguration);

            return services;
        }

       
        public static IServiceCollection AddSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            SwaggerConfiguration swaggerConfiguration = new SwaggerConfiguration();

            configuration.GetSection("AppConfiguration:SwaggerConfiguration").Bind(swaggerConfiguration);
            services.AddSingleton(swaggerConfiguration);
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(setup =>
            {
                // Include 'SecurityScheme' to use JWT Authentication
                var jwtSecurityScheme = new OpenApiSecurityScheme
                {
                    Scheme = "bearer",
                    BearerFormat = "JWT",
                    Name = "JWT Authentication",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",

                    Reference = new OpenApiReference
                    {
                        Id = JwtBearerDefaults.AuthenticationScheme,
                        Type = ReferenceType.SecurityScheme
                    }
                };

                setup.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

                setup.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    { jwtSecurityScheme, Array.Empty<string>() }
                });

            });
            return services;
        }
        public static IApplicationBuilder ConfigureSwagger(this IApplicationBuilder app, SwaggerConfiguration swaggerConfiguration)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = string.Empty;
                c.SwaggerEndpoint($"/swagger/{swaggerConfiguration.Version}/swagger.json", "MavenIndex Auth Api");
               

            });
            return app;
        }
        

    }
}
