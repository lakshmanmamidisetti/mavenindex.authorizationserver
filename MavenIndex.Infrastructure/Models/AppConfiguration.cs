﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenIndex.Infrastructure.Models
{
    public class AppConfiguration
    {
        public string ConnectionString { get; set; }
        public string LogKey { get; set; }

        public string ReportLogoPath { get; set; }
        public string ReportPath { get; set; }
        public string ClientFacingReportServiceUrl { get; set; }
        public string ReportingServiceUrl { get; set; }
        public string ClientFacingReportNames { get; set; }

        public AuthenticationConfiguration AuthenticationConfiguration { get; set; }
        public string ClientUrls { get; set; }


    }
    public class AuthenticationConfiguration
    {
        public string Authority { get; set; }
        public string Audience { get; set; }
        public string Key { get; set; }
        public string ClientUrls { get; set; }
    }

    public class SwaggerConfiguration
    {
        public string Title { get; set; }
        public string TokenApi { get; set; }
        public string Version { get; set; }
    }
}
